package com.lsd.web.persistance.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "OrderInfo")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderInfo {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "productName")
    private String productName;
    @Column(name = "link")
    private String link;
    @Column(name = "image")
    private String image;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "dimensionText")
    private String dimensionText;
    @Column(name = "minPrice")
    private String minPrice;
    @Column(name = "maxPrice")
    private String maxPrice;
    @Column(name = "unitPrice")
    private String unitPrice;
    @Column(name = "address")
    private String address;
    @Column(name = "extraNote")
    private String extraNote;
    @Column(name = "status")
    private String status;
    @Column(name = "orderDate")
    private Date orderDate;
    @Column(name = "userId")
    private Integer userId;
}
