package com.lsd.web.exception;

import com.lsd.lib.exception.ErrorMessage;
import com.lsd.lib.exception.Message;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
public class ErrorInfo extends ErrorMessage {
    private boolean success;

    public ErrorInfo(boolean success) {
        this.success = success;
    }

    public ErrorInfo(String errorCode, String errorDesc, Message message, Object data, boolean success) {
        super(errorCode, errorDesc, message, data);
        this.success = success;
    }
}
