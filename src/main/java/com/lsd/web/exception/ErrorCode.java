package com.lsd.web.exception;

public interface ErrorCode {
    String ERROR_MESSAGE_DEFAULT_EN = "An error occurred. Please try again later";
    String ERROR_MESSAGE_DEFAULT_VN = "Đã có lỗi xảy ra. Vui lòng thử lại sau";
    String SUCCESS = "LSD-00-00";
    String UN_AUTHORIZED = "LSD-00-401";
    String FORBIDDEN = "LSD-00-403";
    String NOT_FOUND = "LSD-00-404";
    String INTERNAL_SERVER = "LSD-00-500";
    String GATEWAY_TIMEOUT = "LSD-00-504";
    String NOTEMPTY = "LSD-00-01";
    String PHONE_OVER= "LSD-00-02";
    String YEAR_ERR= "LSD-00-03";


}
