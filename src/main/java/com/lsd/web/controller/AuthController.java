package com.lsd.web.controller;

import com.lsd.web.exception.ErrorCode;
import com.lsd.web.exception.WebException;
import com.lsd.web.persistance.entity.Admin;
import com.lsd.web.persistance.model.AdminModel;
import com.lsd.web.service.UserService;
import com.lsd.web.util.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(value = "/auth")
//@CrossOrigin(origins = "http://localhost:3000")
public class AuthController {
    @Autowired
    private JwtProvider jwtProvider;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private UserService userService;

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody Admin admin) {
        if(admin.getUsername() != null){
            return new ResponseEntity<>(jwtProvider.generateToken(userService.loadUserByUsername(admin.getUsername())), HttpStatus.OK);
        }
        throw new WebException(ErrorCode.UN_AUTHORIZED);
    }

    @PostMapping(value = "/signup")
    public ResponseEntity<?> signup(@RequestBody AdminModel adminModel) {

        userService.add(adminModel);
        return new ResponseEntity<>(adminModel, HttpStatus.CREATED);
    }
}